package com.hibernatepractice.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.hibernatepractice.entity.Student;

public class CreateStudentDemo {

	public static void main(String args[]) {
		// Create session factory
		SessionFactory factory = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(Student.class)
				.buildSessionFactory();

		// Create session
		Session session = factory.getCurrentSession();

		try {
			// Use the session object to save Java object --
			// Create a Student object
			System.out.println("Creating a new Student object...");
			Student student = new Student("Pratik", "Mulay", "mulaypratik@yahoo.co.in");

			// Start a transaction
			session.beginTransaction();

			// Save the Student object
			System.out.println("Saving the Student...");
			session.save(student);

			// Commit transaction
			session.getTransaction().commit();
			System.out.println("Done!");
			// --
		} finally {
			factory.close();
		}
	}
}
